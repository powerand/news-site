<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_role`.
 * Has foreign keys to the tables:
 *
 * - `user`
 * - `role`
 */
class m170814_191958_create_junction_table_for_user_and_role_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_role', [
            'user_id' => $this->integer(),
            'role_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'PRIMARY KEY(user_id, role_id)',
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-user_role-user_id',
            'user_role',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_role-user_id',
            'user_role',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `role_id`
        $this->createIndex(
            'idx-user_role-role_id',
            'user_role',
            'role_id'
        );

        // add foreign key for table `role`
        $this->addForeignKey(
            'fk-user_role-role_id',
            'user_role',
            'role_id',
            'role',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user_role-user_id',
            'user_role'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-user_role-user_id',
            'user_role'
        );

        // drops foreign key for table `role`
        $this->dropForeignKey(
            'fk-user_role-role_id',
            'user_role'
        );

        // drops index for column `role_id`
        $this->dropIndex(
            'idx-user_role-role_id',
            'user_role'
        );

        $this->dropTable('user_role');
    }
}
