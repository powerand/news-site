# README #

### Для чего создан этот репозиторий? Данный код являеться ответом на задание: ###
	Предложите структуру коллекции MySQL для хранения пользователей сайта c полями: логин, имя, адрес эл. почты, телефон, пароль, список ролей.
	Напишите работающую консольную команду на Yii для смены пароля всем пользователям (100-500 тыс.), используя предложенную коллекцию.

### Как установить? ###

### Системные требования: ###
	CentOS7
	PHP7
	Composer
	Yii2
	MariaDB или MySQL c PDO
	
### Желатеьно наличие ###
	gcc
	git (если нет, файлы можно скачать отсюда: https://drive.google.com/open?id=0BzMeI6qhgrk8a2tRWTRfbjNaN1k)
	nginx

### Конфигурация: ###
	параметры запуска в файле config/params.php

### Конфигурация БД: ###
	имя БД - test
	имя пользователя БД - root
	пароль БД - пустой
	
### Установка: ###
	git clone git@bitbucket.org:powerand/news-site.git && cd news-site
	composer install
	mysql -u root -e "CREATE DATABASE IF NOT EXISTS test DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;"
	php yii migrate --interactive=0
	генерация контента БД:
	а) если установлен gcc (быстрый способ)
		php yii seed/fast
	б) иначе
		php yii seed
	команда на смену пароля всем пользователям:
	php yii user/change-password "new password"
	
### На что должен быть похож результат? ###
	http://liustri.ru/role/
	http://liustri.ru/user/

### С кем я могу поговорить? ###
	artur.yudin.88@gmail.com
	Yudin Artur
