<?php
namespace app\commands;

use yii\console\Controller;

class UserController extends Controller
{
    public function actionChangePassword($value=NULL)
    {
        if(!$value) $value = \Yii::$app->params['defaultPassword'];
        \Yii::$app->db->createCommand()->update('user', ['password' => $value])->execute();
    }
}
?>