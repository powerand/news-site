<?php
namespace app\commands;

use yii\console\Controller;
use app\models\User;
use app\models\Role;
use yii\helpers\Console;

class SeedController extends Controller
{
    public function actionIndex()
    {
        // Заполнение БД тестовыми данными
        $faker = \Faker\Factory::create();

        $user = new User();
        $role = new Role();
        $transaction = \Yii::$app->db->beginTransaction();
        $usersCount = \Yii::$app->params['usersCount'];
        Console::startProgress(0, $usersCount);
        for ( $i = 1; $i <= $usersCount; $i++ )
        {
            $user->setIsNewRecord(true);
            $user->id = null;

            $user->login = $faker->unique()->username;
            $user->name = $faker->firstName;
            $user->email = $faker->email;
            $user->phone = $faker->phoneNumber;
            $user->password = '123456';
            if ( $user->save() )
            {
                if ( $role->id ) $user->link('roles', $role); // Привязываем роль созданную на предидущем этапе для получения 2-х ролей для каждого пользователя
                $role->setIsNewRecord(true);
                $role->id = null;
                $role->name = $faker->company;
                $role->code = 'Role';
                if ( $role->save() ) $user->link('roles', $role);
            }
            Console::updateProgress($i, $usersCount);
        }
        $transaction->commit();
        Console::endProgress();
    }

    public function actionFast()
    {
        // Быстрое заполнение БД тестовыми данными
        $createCsvDir = \Yii::$app->params['createCsvDir'];
        foreach (['user', 'role', 'user_role'] as $model) {
            $testRecordsCount = \Yii::$app->params['testRecordsCount'][$model];
            echo exec("cd ".$createCsvDir."; if [ -f '$model.csv' ]; then echo 'файл уже существует'; else echo 'создаю csv для $model'; gcc $model.c -o $model; ./$model $testRecordsCount; fi")."\n";
            \Yii::$app->db->createCommand("LOAD DATA LOCAL INFILE '".$createCsvDir."$model.csv' INTO TABLE $model CHARACTER SET utf8 FIELDS TERMINATED BY '\t' LINES TERMINATED BY'\n';")->execute();
        }
    }
}
?>