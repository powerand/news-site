<?php

return [
    'adminEmail' => 'artur.yudin.88@gmail.com',
    'usersCount' => 600000,
    'createCsvDir' =>  'vendor/createcsv/',
    'defaultPassword' => '123456',
    'testRecordsCount' => array(
    	'user' => 600000,
    	'role' => 1000,
    	'user_role' => '600000 2',
    )
];
